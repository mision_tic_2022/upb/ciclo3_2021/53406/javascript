


function saludar(){
    console.log("HOLA MUNDO");
}

const sumar = (num1, num2)=>{
    return (num1+num2);
}

const multiplicar = (num1, num2)=>{
    return num1*num2;
}

function operacion(funcion){
    let result = funcion(10,5);
    console.log(result);
}

operacion(sumar);

function validar(cadena){
    let band = false;
    if(cadena.length > 5){
        band = true;
    }
    return band;
}

/*********************************************************************
 * 
 * Crear una función que reciba como parámetro
 * un número, retornar un arreglo con los números 
 * MAYORES al parámetro.
 * [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
 *********************************************************************/

var arreglo = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];

function filtrar_v1(numero){
    let tempoArray = [];

    for(i = 0; i <= arreglo.length; i++){
        if(arreglo[i]>numero){
            tempoArray.push(arreglo[i]);
        }
    }
    return tempoArray;
}

const filtrar_v2 = (numero)=>{
    let tempoArray = [];

    for(i = 0; i <= arreglo.length; i++){
        if(arreglo[i]>numero){
            tempoArray.push(arreglo[i]);
        }
    }
    return tempoArray;
}

function filtrar_v3(numero){
    let result = arreglo.filter(n => (n > numero));
    return result;
}

module.exports = {
    saludar,
    sumar,
    multiplicar,
    validar,
    filtrar_v1,
    filtrar_v2,
    filtrar_v3
};

//exports.default = MiClase;
